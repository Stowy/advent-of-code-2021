mod board;
pub mod consts;

use board::Board;
use std::fs;

const FILENAME: &str = "input.txt";
const BOARD_SIZE: usize = 5;

fn main() {
    // Read the file
    let content = fs::read_to_string(FILENAME).expect("Error opening the input file.");

    // Split the file by lines
    let mut lines: Vec<&str> = content.lines().collect();
    let mut lines_part_two = lines.to_vec();
    part_one(&mut lines);
    part_two(&mut lines_part_two);
}

fn part_one(input: &mut Vec<&str>) {
    // Split and convert the numbers to i32
    let numbers_to_draw = input[0].split(',').map(|num| num.parse::<i32>().unwrap());

    let mut boards: Vec<Board> = Vec::new();

    // Remove the lines that are not needed anymore
    input.remove(0);
    input.remove(0);

    // Read all the boards
    while input.len() >= BOARD_SIZE {
        {
            // Get the board from the vec
            let mut board_data = input.drain(0..BOARD_SIZE);

            boards.push(Board::new(&mut board_data));
        }
        // remove empty line
        if input.len() > 0 {
            input.remove(0);
        }
    }

    // Draw the number
    for number_to_draw in numbers_to_draw {
        for board in &mut boards {
            board.draw(number_to_draw);
            if board.is_winning() {
                println!(
                    "Final score part one is : {}",
                    board.get_score(number_to_draw)
                );
                return;
            }
        }
    }
}

fn part_two(input: &mut Vec<&str>) {
    // Split and convert the numbers to i32
    let numbers_to_draw = input[0].split(',').map(|num| num.parse::<i32>().unwrap());

    let mut boards: Vec<Board> = Vec::new();

    // Remove the lines that are not needed anymore
    input.remove(0);
    input.remove(0);

    // Read all the boards
    while input.len() >= BOARD_SIZE {
        {
            // Get the board from the vec
            let mut board_data = input.drain(0..BOARD_SIZE);

            boards.push(Board::new(&mut board_data));
        }
        // remove empty line
        if input.len() > 0 {
            input.remove(0);
        }
    }

    let mut winning_boards: Vec<(Board, i32)> = Vec::new();

    // Draw the number
    for number_to_draw in numbers_to_draw {
        let mut i = 0;
        while i < boards.len() {
            boards[i].draw(number_to_draw);
            if boards[i].is_winning() {
                winning_boards.push((boards[i], number_to_draw));
                boards.remove(i);
            } else {
                i += 1;
            }
        }
    }

    let (last_board, last_draw) = winning_boards.last().unwrap();

    println!(
        "Part two: Last winning board score is : {}",
        last_board.get_score(*last_draw)
    );
}
