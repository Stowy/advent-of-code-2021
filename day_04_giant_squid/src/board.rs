use crate::consts::*;

#[derive(Debug, Copy, Clone)]
pub struct Board {
    data: [[i32; BOARD_SIZE]; BOARD_SIZE],
    drawn_numbers: [[bool; BOARD_SIZE]; BOARD_SIZE],
}

impl Board {
    pub fn new(data: &mut std::vec::Drain<&str>) -> Board {
        let mut new_data: [[i32; BOARD_SIZE]; BOARD_SIZE] = [[0; BOARD_SIZE]; BOARD_SIZE];
        for i in 0..BOARD_SIZE {
            let line = data.next().unwrap();
            let mut splitted_line = line.split_whitespace();
            for j in 0..BOARD_SIZE {
                let num_str = splitted_line.next().unwrap();
                new_data[i][j] = num_str.parse::<i32>().unwrap();
            }
        }

        Board {
            data: new_data,
            drawn_numbers: [[false; BOARD_SIZE]; BOARD_SIZE],
        }
    }

    pub fn draw(&mut self, num: i32) {
        for i in 0..BOARD_SIZE {
            for j in 0..BOARD_SIZE {
                if self.data[i][j] == num {
                    self.drawn_numbers[i][j] = true;
                }
            }
        }
    }

    pub fn is_winning(&self) -> bool {
        // Check collumn
        for line in self.drawn_numbers {
            let mut col_count: usize = 0;
            for is_drawn in line {
                col_count += is_drawn as usize;
            }
            if col_count == self.drawn_numbers.len() {
                return true;
            }
        }
        // Check line
        let col_size = self.drawn_numbers.len();
        for i in 0..col_size {
            let mut line_count: usize = 0;
            for j in 0..self.drawn_numbers.len() {
                line_count += self.drawn_numbers[j][i] as usize;
            }
            if line_count >= col_size {
                return true;
            }
        }
        false
    }

    pub fn get_score(&self, called_num: i32) -> i32 {
        let mut undrawn_sum = 0;
        for i in 0..BOARD_SIZE {
            for j in 0..BOARD_SIZE {
                let is_drawn = self.drawn_numbers[i][j];
                if !is_drawn {
                    undrawn_sum += self.data[i][j];
                }
            }
        }

        undrawn_sum * called_num
    }
}
