use std::fs;

const FILENAME: &str = "input.txt";
const GRID_SIZE: usize = 10;
// Change NBR_STEPS to 100 for part 1
const NBR_STEPS: usize = 349;
const MAX_ENERGY_LEVEL: i32 = 9;

fn main() {
    // Read the file
    let content = fs::read_to_string(FILENAME).expect("Error opening the input file.");

    // Split the file by lines
    let lines: Vec<&str> = content.lines().collect();
    let start_time = std::time::Instant::now();
    solve_part_one(lines);
    let time1ns = start_time.elapsed().as_nanos();
    let time1ms = start_time.elapsed().as_millis();
    println!("Part 1 = {} ns, {} ms\n", time1ns, time1ms);
}

fn solve_part_one(input: Vec<&str>) {
    let mut octopuses: [[i32; GRID_SIZE]; GRID_SIZE] = parse_octopuses(input);

    // Flash octopuses
    let mut total_flash_counts = 0;
    for step in 0..NBR_STEPS {
        // Increase energy level of each octopuses by 1
        for line in &mut octopuses {
            for octopus in &mut *line {
                *octopus += 1;
            }
        }

        // Flash octopuses that are above 9
        let mut flash_history: [[bool; GRID_SIZE]; GRID_SIZE] = [[false; GRID_SIZE]; GRID_SIZE];
        let mut flash_count: i32 = 0;
        for x in 0..octopuses.len() {
            for y in 0..octopuses[x].len() {
                if octopuses[x][y] > MAX_ENERGY_LEVEL {
                    if !flash_history[x][y] {
                        flash(&mut octopuses, x, y, &mut flash_history, &mut flash_count);
                    }
                }
            }
        }
        total_flash_counts += flash_count;

        // Set to 0 every octopus that flashed
        for x in 0..octopuses.len() {
            for y in 0..octopuses[x].len() {
                if flash_history[x][y] {
                    octopuses[x][y] = 0;
                }
            }
        }

        let mut all_flash = true;
        for x in 0..GRID_SIZE {
            for y in 0..GRID_SIZE {
                all_flash = all_flash && flash_history[x][y];
            }
        }
        if all_flash {
            println!("Part two, all flash on step : {}", step + 1);
        }
    }

    println!("Part one, flash count : {}", total_flash_counts);
}

fn flash(
    octopuses: &mut [[i32; GRID_SIZE]; GRID_SIZE],
    x: usize,
    y: usize,
    flash_history: &mut [[bool; GRID_SIZE]; GRID_SIZE],
    flash_count: &mut i32,
) {
    // Flash the current octopus
    flash_history[x][y] = true;
    *flash_count += 1;

    // Flash the surrounding octopusses
    let x = x as i32;
    let y = y as i32;
    for i in (x - 1)..=(x + 1) {
        for j in (y - 1)..=(y + 1) {
            // Check to not flash the current point
            if i == x && j == y {
                continue;
            }

            // Check to not be out of bounds
            if i < 0 || i >= GRID_SIZE as i32 || j < 0 || j >= GRID_SIZE as i32 {
                continue;
            }

            // Increment surrounding
            let i = i as usize;
            let j = j as usize;
            octopuses[i][j] += 1;
            if octopuses[i][j] > MAX_ENERGY_LEVEL {
                // Check if not already flashed
                if !flash_history[i][j] {
                    // Flash the octopus
                    flash(octopuses, i, j, flash_history, flash_count);
                }
            }
        }
    }
}

fn parse_octopuses(input: Vec<&str>) -> [[i32; GRID_SIZE]; GRID_SIZE] {
    let mut octopuses: [[i32; GRID_SIZE]; GRID_SIZE] = [[0; GRID_SIZE]; GRID_SIZE];

    for y in 0..input.len() {
        let mut chars = input[y].chars();
        for x in 0..input[y].chars().count() {
            let next_char = chars.next().unwrap();
            octopuses[x][y] = next_char.to_digit(10).unwrap() as i32;
        }
    }

    octopuses
}
