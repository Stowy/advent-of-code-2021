pub struct Heightmap {
    pub size_x: usize,
    pub size_y: usize,
    pub data: Vec<Vec<i32>>,
}

#[derive(Debug, Copy, Clone, Eq, Hash)]
pub struct Point {
    pub x: usize,
    pub y: usize,
}

impl PartialEq for Point {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

impl Heightmap {
    pub fn from_strings(strings: Vec<&str>) -> Heightmap {
        let size_x = strings[0].len();
        let size_y = strings.len();
        let mut new_data: Vec<Vec<i32>> = vec![vec![0; size_y]; size_x];

        // Parse heightmap
        for y in 0..size_y {
            let line = strings[y];
            for x in 0..size_x {
                let character = line.chars().nth(x).unwrap();
                new_data[x][y] = character.to_digit(10).unwrap() as i32;
            }
        }

        Heightmap {
            size_x: size_x,
            size_y: size_y,
            data: new_data,
        }
    }

    pub fn at(&self, point: Point) -> i32 {
        self.data[point.x][point.y]
    }

    pub fn find_lowest(&self, point: Point) -> Option<Point> {
        let mut has_value = false;
        let current_value = self.at(point);
        let mut lowest_point = point;

        // Find lowest point
        if point.x > 0 {
            let left_point = Point {
                x: point.x - 1,
                y: point.y,
            };
            if !has_value {
                has_value = true;
                lowest_point = left_point;
            } else {
                let left = self.at(left_point);
                if left < self.at(lowest_point) {
                    lowest_point = left_point;
                }
            }
        }
        if point.x < self.size_x - 1 {
            let right_point = Point {
                x: point.x + 1,
                y: point.y,
            };
            if !has_value {
                has_value = true;
                lowest_point = right_point;
            } else {
                let right = self.at(right_point);
                if right < self.at(lowest_point) {
                    lowest_point = right_point;
                }
            }
        }
        if point.y > 0 {
            let up_point = Point {
                x: point.x,
                y: point.y - 1,
            };
            if !has_value {
                has_value = true;
                lowest_point = up_point;
            } else {
                let up = self.at(up_point);
                if up < self.at(lowest_point) {
                    lowest_point = up_point;
                }
            }
        }
        if point.y < self.size_y - 1 {
            let right_point = Point {
                x: point.x,
                y: point.y + 1,
            };
            if !has_value {
                lowest_point = right_point;
            } else {
                let right = self.at(right_point);
                if right < self.at(lowest_point) {
                    lowest_point = right_point;
                }
            }
        }

        let lowest_value = self.at(lowest_point);
        if lowest_value == current_value {
            return None;
        } else if lowest_value < current_value {
            return self.find_lowest(lowest_point);
        } else {
            return Some(point);
        }
    }
}
