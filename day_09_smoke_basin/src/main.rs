mod heightmap;

use heightmap::Heightmap;
use heightmap::Point;
use std::collections::HashMap;
use std::fs;

const FILENAME: &str = "input.txt";

fn main() {
    // Read the file
    let content = fs::read_to_string(FILENAME).expect("Error opening the input file.");

    // Split the file by lines
    let lines: Vec<&str> = content.lines().collect();
    let lines2 = lines.clone();
    let start_time = std::time::Instant::now();
    solve_part_one(lines);
    let time1ns = start_time.elapsed().as_nanos();
    let time1ms = start_time.elapsed().as_millis();
    println!("Part 1 = {} ns, {} ms\n", time1ns, time1ms);

    let start_time = std::time::Instant::now();
    solve_part_two(lines2);
    let time2ns = start_time.elapsed().as_nanos();
    let time2ms = start_time.elapsed().as_millis();
    println!("Part 2 = {} ns, {} ms", time2ns, time2ms);
}

fn solve_part_one(input: Vec<&str>) {
    let heightmap: Heightmap = Heightmap::from_strings(input);
    // let mut basins: HashMap<Point, i32> = HashMap::new();

    let mut lowests: Vec<Point> = Vec::new();
    for x in 0..heightmap.size_x {
        for y in 0..heightmap.size_y {
            let point = Point { x: x, y: y };
            let lowest = heightmap.find_lowest(point);
            if lowest.is_some() {
                let lowest = lowest.unwrap();
                if !lowests.contains(&lowest) {
                    lowests.push(lowest);
                }
            }
        }
    }

    let mut sum = 0;
    for point in &lowests {
        let my_point = *point;
        let risk_level = heightmap.at(my_point) + 1;
        sum += risk_level;
    }
    println!("Part one sum : {}", sum);
}

fn solve_part_two(input: Vec<&str>) {
    let heightmap: Heightmap = Heightmap::from_strings(input);
    let mut basins: HashMap<Point, i32> = HashMap::new();

    for x in 0..heightmap.size_x {
        for y in 0..heightmap.size_y {
            let point = Point { x: x, y: y };
            let lowest = heightmap.find_lowest(point);
            if lowest.is_some() {
                let lowest = lowest.unwrap();
                if heightmap.at(point) != 9 {
                    let entry = basins.entry(lowest).or_insert(0);
                    *entry += 1;
                }
            }
        }
    }

    let mut three_biggest: Vec<i32> = Vec::new();
    for (_, size) in &basins {
        if three_biggest.len() < 3 {
            three_biggest.push(*size);
        } else {
            // Check if one elements is smaller than the size
            if !three_biggest.iter().all(|&num| num > *size) {
                // Remove the smallest element and add the size
                let smallest = three_biggest.iter().min().unwrap();
                let smallest_pos = three_biggest
                    .iter()
                    .position(|num| *num == *smallest)
                    .unwrap();
                three_biggest.remove(smallest_pos);
                three_biggest.push(*size);
            }
        }
    }

    let mut product = 1;
    for size in three_biggest {
        product *= size;
    }
    println!("Part two sizes : {}", product);
}
