use crate::Point;

#[derive(Debug, Clone, Copy)]
pub struct Line {
    pub start: Point,
    pub end: Point,
}

impl Line {
    pub fn new(start: Point, end: Point) -> Line {
        Line {
            start: start,
            end: end,
        }
    }

    pub fn from_string(str_line: &str) -> Line {
        let mut splitted = str_line.split(" -> ");
        let str_begin = splitted.next().unwrap();
        let str_end = splitted.next().unwrap();
        let mut splitted_begin = str_begin.split(',');
        let mut splitted_end = str_end.split(',');
        let str_x1 = splitted_begin.next().unwrap();
        let str_x2 = splitted_begin.next().unwrap();
        let str_y1 = splitted_end.next().unwrap();
        let str_y2 = splitted_end.next().unwrap();
        let x1 = str_x1.parse::<i32>().unwrap();
        let x2 = str_x2.parse::<i32>().unwrap();
        let y1 = str_y1.parse::<i32>().unwrap();
        let y2 = str_y2.parse::<i32>().unwrap();

        Line::new(Point::new(x1, x2), Point::new(y1, y2))
    }
}
