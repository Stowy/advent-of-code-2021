use crate::Line;
use std::fmt;

// #[derive(Debug)]
pub struct World {
    data: Vec<Vec<i32>>,
}

impl World {
    pub fn new(size_x: usize, size_y: usize) -> World {
        let mut data: Vec<Vec<i32>> = Vec::new();
        data.reserve(size_x);
        for _ in 0..size_x as usize {
            data.push(vec![0; size_y]);
        }

        World { data: data }
    }

    pub fn draw_line(&mut self, line: Line) {
        let mut size_start_x = line.start.x as usize;
        let mut size_start_y = line.start.y as usize;
        let mut size_end_x = line.end.x as usize;
        let mut size_end_y = line.end.y as usize;

        if size_start_x < size_end_x && size_start_y > size_end_y {
            // Going up right, y goes down
            let mut x: i32 = line.start.x;
            let mut y: i32 = line.start.y;
            while x <= line.end.x && y >= line.end.y {
                self.data[x as usize][y as usize] += 1;
                x += 1;
                y -= 1;
            }
        } else if size_start_x > size_end_x && size_start_y < size_end_y {
            // Going down left, x goes down
            let mut x: i32 = line.start.x;
            let mut y: i32 = line.start.y;
            while x >= line.end.x && y <= line.end.y {
                self.data[x as usize][y as usize] += 1;
                x -= 1;
                y += 1;
            }
        } else if size_start_x > size_end_x && size_start_y > size_end_y {
            // Going up left, x and y goes down
            let mut x: i32 = line.start.x;
            let mut y: i32 = line.start.y;
            while x >= line.end.x && y >= line.end.y {
                self.data[x as usize][y as usize] += 1;
                x -= 1;
                y -= 1;
            }
        } else if size_end_x > size_start_x && size_end_y > size_start_y {
            // Going down left, x and y goes up
            let mut x: i32 = line.start.x;
            let mut y: i32 = line.start.y;
            while x <= line.end.x && y <= line.end.y {
                self.data[x as usize][y as usize] += 1;
                x += 1;
                y += 1;
            }
        } else if line.start.x == line.end.x {
            // Swap the var if start is above end (not permited by rust range)
            if size_start_y > size_end_y {
                let temp = size_start_y;
                size_start_y = size_end_y;
                size_end_y = temp;
            }
            // Draw horizontal line
            for y in size_start_y..=size_end_y {
                self.data[size_start_x][y] += 1;
            }
        } else if line.start.y == line.end.y {
            // Swap the var if start is above end (not permited by rust range)
            if size_start_x > size_end_x {
                let temp = size_start_x;
                size_start_x = size_end_x;
                size_end_x = temp;
            }
            // Draw vertical line
            for x in size_start_x..=size_end_x {
                self.data[x][size_start_y] += 1;
            }
        }
    }

    pub fn get_num_overlapping_points(&self) -> i32 {
        let mut counter = 0;
        for line in &self.data {
            for point in line {
                if *point >= 2 {
                    counter += 1;
                }
            }
        }
        counter
    }
}

impl fmt::Display for World {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut output = String::new();
        // Check if data is empty
        if self.data.len() == 0 {
            return write!(f, "");
        }

        // Print the data
        for y in 0..self.data[0].len() {
            for x in 0..self.data.len() {
                if self.data[x][y] == 0 {
                    output.push('.');
                } else {
                    output.push_str(&self.data[x][y].to_string());
                }
            }
            output.push('\n');
        }
        write!(f, "{}", output)
    }
}
