mod line;
mod point;
mod world;

use line::Line;
use point::Point;
use std::fs;
use world::World;

const FILENAME: &str = "input.txt";

fn main() {
    // Read the file
    let content = fs::read_to_string(FILENAME).expect("Error opening the input file.");

    // Split the file by lines
    let lines: Vec<&str> = content.lines().collect();
    solve_part_one(lines);
}

fn solve_part_one(input: Vec<&str>) {
    let mut lines: Vec<Line> = Vec::new();
    let mut biggest_x = 0;
    let mut biggest_y = 0;
    for str_line in input {
        let line = Line::from_string(str_line);

        // Save the biggest value to know the size of the world
        if line.start.x > biggest_x {
            biggest_x = line.start.x;
        }
        if line.start.y > biggest_x {
            biggest_x = line.start.y;
        }
        if line.end.x > biggest_y {
            biggest_y = line.end.x;
        }
        if line.end.y > biggest_y {
            biggest_y = line.end.y;
        }

        lines.push(line);
    }

    // Convert the size of the world in usize
    let size_x: usize = (biggest_x as usize) + 1;
    let size_y: usize = (biggest_y as usize) + 1;

    // Create and initialise the world
    let mut world: World = World::new(size_x, size_y);

    for line in lines {
        world.draw_line(line);
    }
    // world.draw_line(lines[5]);

    // println!("{}", world);
    println!(
        "Part one\nNum overlapping points : {}",
        world.get_num_overlapping_points()
    );
}
