use std::fs;

const FILENAME: &str = "input.txt";
const BIT_COUNT: usize = 12;

fn main() {
    part_one();
    part_two();
}

fn part_one() {
    // Read the file
    let content = fs::read_to_string(FILENAME).expect("Error opening the input file.");

    // Split the file by lines
    let lines = content.lines();

    // Create vars
    let mut sum_bits: [i32; BIT_COUNT] = [0; BIT_COUNT];
    let mut gamma_rate_string = String::new();
    let mut epsilon_rate_string = String::new();
    let mut nbr_lines: usize = 0;

    // Count the bits in each lines
    for line in lines {
        for i in 0..line.chars().count() {
            let character = line.chars().nth(i).expect("Error reading char");
            if character == '1' {
                sum_bits[i] += 1;
            }
        }
        nbr_lines += 1;
    }

    let half_nbr_line = (nbr_lines / 2) as i32;

    // Find the most found bit
    for sum_bit in sum_bits {
        if sum_bit > half_nbr_line {
            gamma_rate_string.push('1');
            epsilon_rate_string.push('0');
        } else {
            gamma_rate_string.push('0');
            epsilon_rate_string.push('1');
        }
    }

    let gamma_rate = i32::from_str_radix(&gamma_rate_string, 2).unwrap();
    let epsilon_rate = i32::from_str_radix(&epsilon_rate_string, 2).unwrap();

    println!(
        "Part one : \nPower consumption is : {}",
        gamma_rate * epsilon_rate
    );
}

fn part_two() {
    // Read the file
    let content = fs::read_to_string(FILENAME).expect("Error opening the input file.");

    // Split the file by lines
    let lines: Vec<&str> = content.lines().collect();

    let oxygen_generator_rating_string = find_oxygen_generator_rating(lines.clone(), 0)[0];
    let co2_scrubber_rating_string = find_co2_scrubber_rating(lines, 0)[0];

    let oxygen_generator_rating = i32::from_str_radix(&oxygen_generator_rating_string, 2).unwrap();
    let co2_scrubber_rating = i32::from_str_radix(&co2_scrubber_rating_string, 2).unwrap();

    println!("\nPart Two");
    println!("Oxygen Generator Rating : {}", oxygen_generator_rating);
    println!("CO2 Scrubber Rating : {}", co2_scrubber_rating);
    println!(
        "Life Support Rating : {}",
        oxygen_generator_rating * co2_scrubber_rating
    );
}

fn find_oxygen_generator_rating(values: Vec<&str>, pos: usize) -> Vec<&str> {
    // If we have one value left or not more bits to check, return
    if values.len() == 1 || pos >= BIT_COUNT {
        return values;
    }

    let mut one_count = 0;
    let mut zero_count = 0;

    // Count the bits at the pos
    for value in values.iter() {
        let current_char = value.chars().nth(pos).unwrap();
        if current_char == '1' {
            one_count += 1;
        } else if current_char == '0' {
            zero_count += 1;
        }
    }

    // Check whether 1 or 0 is the most frequent
    let mut new_values: Vec<&str> = Vec::new();

    // Count the bits at the pos
    for value in values.iter() {
        let char_to_find = if one_count >= zero_count { '1' } else { '0' };
        if value.chars().nth(pos).unwrap() == char_to_find {
            new_values.push(value);
        }
    }

    find_oxygen_generator_rating(new_values, pos + 1)
}

fn find_co2_scrubber_rating(values: Vec<&str>, pos: usize) -> Vec<&str> {
    // If we have one value left or not more bits to check, return
    if values.len() == 1 || pos >= BIT_COUNT {
        return values;
    }

    let mut one_count = 0;
    let mut zero_count = 0;

    // Count the bits at the pos
    for value in values.iter() {
        let current_char = value.chars().nth(pos).unwrap();
        if current_char == '1' {
            one_count += 1;
        } else if current_char == '0' {
            zero_count += 1;
        }
    }

    // Check whether 1 or 0 is the most frequent
    let mut new_values: Vec<&str> = Vec::new();

    // Count the bits at the pos
    for value in values.iter() {
        let char_to_find = if one_count < zero_count { '1' } else { '0' };
        if value.chars().nth(pos).unwrap() == char_to_find {
            new_values.push(value);
        }
    }

    find_co2_scrubber_rating(new_values, pos + 1)
}
