use std::fs;

const FILENAME: &str = "input.txt";

fn main() {
    // Read the file
    let content = fs::read_to_string(FILENAME).expect("Error opening the input file.");

    // Split the file by lines
    let lines: Vec<&str> = content.lines().collect();
    let lines2 = lines.clone();
    let start_time = std::time::Instant::now();
    solve_part_one(lines);
    let time1ns = start_time.elapsed().as_nanos();
    let time1ms = start_time.elapsed().as_millis();
    println!("Part 1 = {} ns, {} ms", time1ns, time1ms);

    let start_time = std::time::Instant::now();
    solve_part_two(lines2);
    let time2ns = start_time.elapsed().as_nanos();
    let time2ms = start_time.elapsed().as_millis();
    println!("Part 2 = {} ns, {} ms", time2ns, time2ms);
}

fn solve_part_one(input: Vec<&str>) {
    // Convert input to i32
    let splitted_input = input[0].split(',');
    let horizontal_positions: Vec<i32> = splitted_input
        .map(|input_str| input_str.parse::<i32>().unwrap())
        .collect();

    let mut min_fuel_cost = 0;
    let mut best_pos = 0;
    let biggest = horizontal_positions.iter().max().unwrap();
    for new_hpos in 0..=*biggest {
        let fuel_cost = compute_fuel_cost(new_hpos, &horizontal_positions);
        if min_fuel_cost == 0 || min_fuel_cost > fuel_cost {
            min_fuel_cost = fuel_cost;
            best_pos = new_hpos;
        }
    }

    println!("Best pos is : {}\nFuel cost : {}", best_pos, min_fuel_cost);
}

fn compute_fuel_cost(new_hpos: i32, horizontal_positions: &Vec<i32>) -> i32 {
    let mut total_fuel_cost = 0;
    for hpos in horizontal_positions {
        total_fuel_cost += (hpos - new_hpos).abs();
    }

    total_fuel_cost
}

fn solve_part_two(input: Vec<&str>) {
    // Convert input to i32
    let splitted_input = input[0].split(',');
    let horizontal_positions: Vec<i32> = splitted_input
        .map(|input_str| input_str.parse::<i32>().unwrap())
        .collect();

    let mut min_fuel_cost = 0;
    let mut best_pos = 0;
    let biggest = horizontal_positions.iter().max().unwrap();
    for new_hpos in 0..=*biggest {
        let fuel_cost = compute_fuel_cost_part2(new_hpos, &horizontal_positions);
        if min_fuel_cost == 0 || min_fuel_cost > fuel_cost {
            min_fuel_cost = fuel_cost;
            best_pos = new_hpos;
        }
    }

    println!("Best pos is : {}\nFuel cost : {}", best_pos, min_fuel_cost);
}

fn compute_fuel_cost_part2(new_hpos: i32, horizontal_positions: &Vec<i32>) -> i32 {
    let mut total_fuel_cost = 0;
    for hpos in horizontal_positions {
        let movement_ammount = (hpos - new_hpos).abs();
        total_fuel_cost += compute_triangular_number(movement_ammount);
    }

    total_fuel_cost
}

fn compute_triangular_number(n: i32) -> i32 {
    (n * n + n) / 2
}
