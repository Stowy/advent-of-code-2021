#[macro_use]
extern crate lazy_static;

use std::collections::HashMap;
use std::fs;

const FILENAME: &str = "input.txt";
const POSSIBLE_CHUNKS: [(char, char); 4] = [('(', ')'), ('[', ']'), ('{', '}'), ('<', '>')];

lazy_static! {
    static ref SCORES: HashMap<char, i32> =
        HashMap::from([(')', 3), (']', 57), ('}', 1197), ('>', 25137)]);
}

fn main() {
    // Read the file
    let content = fs::read_to_string(FILENAME).expect("Error opening the input file.");

    // Split the file by lines
    let lines: Vec<&str> = content.lines().collect();
    let lines2 = lines.clone();
    let start_time = std::time::Instant::now();
    solve_part_one(lines);
    let time1ns = start_time.elapsed().as_nanos();
    let time1ms = start_time.elapsed().as_millis();
    println!("Part 1 = {} ns, {} ms\n", time1ns, time1ms);

    let start_time = std::time::Instant::now();
    solve_part_two(lines2);
    let time2ns = start_time.elapsed().as_nanos();
    let time2ms = start_time.elapsed().as_millis();
    println!("Part 2 = {} ns, {} ms", time2ns, time2ms);
}

fn solve_part_one(input: Vec<&str>) {
    let mut synthax_error_score = 0;
    for line in input {
        let corrupted = check_corrupted(line);
        match corrupted {
            Ok(character) => synthax_error_score += SCORES.get(&character).unwrap(),
            Err(_) => {}
        }
    }

    println!("Final synthax error score : {}", synthax_error_score);
}

fn check_corrupted(line: &str) -> Result<char, &str> {
    let mut bracket_stack: Vec<char> = Vec::new();
    for character in line.chars() {
        for possible_chunk in POSSIBLE_CHUNKS {
            if character == possible_chunk.0 {
                bracket_stack.push(character);
            } else if character == possible_chunk.1 {
                let poped_char = bracket_stack.pop().unwrap();
                if poped_char != possible_chunk.0 {
                    return Ok(character);
                }
            }
        }
    }

    Err("Nothing wrong")
}

fn solve_part_two(input: Vec<&str>) {
    let mut scores: Vec<i64> = Vec::new();
    for line in input {
        let autocomplete = autocomplete(line);
        match autocomplete {
            Ok(completed) => scores.push(compute_score(completed)),
            Err(_) => {}
        }
    }

    let median = median(&mut scores);
    println!("Part 2 median score : {}", median);
}

pub fn median(tab: &mut Vec<i64>) -> i64 {
    tab.sort();
    let mid_index = tab.len() / 2;

    tab[mid_index]
}

fn compute_score(completed: String) -> i64 {
    let mut total_score: i64 = 0;
    for character in completed.chars() {
        total_score *= 5;
        match character {
            ')' => total_score += 1,
            ']' => total_score += 2,
            '}' => total_score += 3,
            '>' => total_score += 4,
            _ => {}
        }
    }

    total_score
}

fn autocomplete(string: &str) -> Result<String, &str> {
    let mut bracket_stack: Vec<char> = Vec::new();
    for character in string.chars() {
        for possible_chunk in POSSIBLE_CHUNKS {
            if character == possible_chunk.0 {
                bracket_stack.push(character);
            } else if character == possible_chunk.1 {
                let poped_char = bracket_stack.pop().unwrap();
                if poped_char != possible_chunk.0 {
                    return Err("Corrupted");
                }
            }
        }
    }

    let mut completion_string = String::new();
    for bracket in bracket_stack.iter().rev() {
        match bracket {
            '(' => completion_string.push(')'),
            '[' => completion_string.push(']'),
            '{' => completion_string.push('}'),
            '<' => completion_string.push('>'),
            _ => {}
        }
    }

    Ok(completion_string)
}
