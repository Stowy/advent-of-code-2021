use std::fs::File;
use std::io::{BufRead, BufReader};

const FILENAME: &str = "input.txt";

fn main() {
    part_one();
    part_two();
}

fn part_one() {
    let mut is_first_input: bool = true;
    let mut old_input: i32;
    let mut larger_count = 0;
    let mut input = 0;
    let file = File::open(FILENAME).expect("Error opening the input file");
    let reader = BufReader::new(file);

    for (_, line) in reader.lines().enumerate() {
        // Check line for errors
        let line = line.expect("Error reading the line");

        // Save the last input
        old_input = input;

        // Break if line is empty
        if line == "" {
            break;
        }

        // Convert the input to i32
        input = line
            .parse::<i32>()
            .expect("Error converting the string to i32");

        // Check if this is the first input
        if is_first_input {
            // Skip if it's the first input, and set it to false
            is_first_input = false;
            continue;
        } else {
            // If the input is bigger than the old one, we increment
            if input > old_input {
                larger_count += 1;
            }
        }
    }

    println!("Part one count : {}", larger_count);
}

fn part_two() {
    let mut first_done = false;
    let mut second_done = false;
    let file = File::open(FILENAME).expect("Error opening the input file");
    let reader = BufReader::new(file);
    let mut sums: Vec<i32> = vec![];
    let mut input: i32;

    // Read all the sums
    for (index, line) in reader.lines().enumerate() {
        // Check line for errors
        let line = line.expect("Error reading the line");
        // Break if line is empty
        if line == "" {
            break;
        }

        // Convert the input to i32
        input = line
            .parse::<i32>()
            .expect("Error converting the string to i32");

        sums.push(input);

        if first_done {
            sums[index - 1] += input;
        }

        if second_done {
            sums[index - 2] += input;
        }

        if !first_done {
            first_done = true;
        } else if !second_done && first_done {
            second_done = true;
        }
    }

    let mut old_sum = 0;
    let mut first_sum_done = false;
    let mut larger_count = 0;

    for sum in sums {
        // Check if this is the first sum
        if !first_sum_done {
            // Skip if it's the first sum
            first_sum_done = true;
            old_sum = sum;
            continue;
        } else {
            // If the input is bigger than the old one, we increment
            if sum > old_sum {
                larger_count += 1;
            }
            old_sum = sum;
        }
    }

    println!("Part two count : {}", larger_count);
}
