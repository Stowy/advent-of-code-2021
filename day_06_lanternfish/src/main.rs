use std::fs;

const FILENAME: &str = "input.txt";
const NBR_DAYS: usize = 256;
const MAX_AGE: usize = 6;
const GROWTH_TIME: usize = 2;
const MAX_TIME: usize = MAX_AGE + GROWTH_TIME;

fn main() {
    // Read the file
    let content = fs::read_to_string(FILENAME).expect("Error opening the input file.");

    // Split the file by lines
    let lines: Vec<&str> = content.lines().collect();
    let start_time = std::time::Instant::now();
    solve_part_two(lines);
    let time1 = start_time.elapsed().as_nanos();
    println!("Part1 = ({} ns)", time1);
}

fn solve_part_two(input: Vec<&str>) {
    let line = input[0];
    let split = line.split(',');
    let mut fishes: [i64; MAX_TIME + 1] = [0; MAX_TIME + 1];

    // Parse fishes in vec
    for num in split {
        let parsed = num.parse::<usize>();
        match parsed {
            Ok(val) => fishes[val] += 1,
            Err(_) => println!("NO fish"),
        }
    }

    // Grow each day
    let mut last_value: i64 = *fishes.last().unwrap();
    for _ in 0..NBR_DAYS {
        for i in (0..MAX_TIME + 1).rev() {
            if i == 0 {
                fishes[6] += last_value;
                fishes[8] += last_value;
            } else {
                let temp = fishes[i - 1];
                fishes[i - 1] = last_value;
                last_value = temp;
                if i == MAX_TIME {
                    fishes[MAX_TIME] = 0;
                }
            }
        }
    }

    let fish_count: i64 = get_num_fish(fishes);

    println!("Number of fishes after {} days : {}", NBR_DAYS, fish_count);
}

fn get_num_fish(fishes: [i64; MAX_TIME + 1]) -> i64 {
    let mut fish_count: i64 = 0;
    for num_fish in fishes {
        fish_count += num_fish;
    }

    fish_count
}
