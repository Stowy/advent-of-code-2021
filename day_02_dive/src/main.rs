mod new_submarine;
mod submarine;

use new_submarine::NewSubmarine;
use std::fs::File;
use std::io::{BufRead, BufReader};
use submarine::Submarine;

const FILENAME: &str = "input.txt";
const FORWARD_TXT: &str = "forward";
const DOWN_TXT: &str = "down";
const UP_TXT: &str = "up";

fn main() {
    part_one();
    part_two();
}

fn part_one() {
    let mut submarine = Submarine::new();
    let file = File::open(FILENAME).expect("Error opening the input file");
    let reader = BufReader::new(file);

    for (_index, line) in reader.lines().enumerate() {
        // Check line for errors
        let line = line.expect("Error reading the line");

        let split = line.split(" ");
        let text_vec: Vec<&str> = split.collect();
        let instruction = text_vec[0];
        let ammount: i32 = text_vec[1].parse().expect("Error parsing input number");
        match instruction {
            FORWARD_TXT => submarine.forward(ammount),
            DOWN_TXT => submarine.down(ammount),
            UP_TXT => submarine.up(ammount),
            _ => println!("Instruction not recognised : {}", instruction),
        }
    }

    println!("Mult part one : {}", submarine.get_mult());
}

fn part_two() {
    let mut submarine = NewSubmarine::new();
    let file = File::open(FILENAME).expect("Error opening the input file");
    let reader = BufReader::new(file);

    for (_index, line) in reader.lines().enumerate() {
        // Check line for errors
        let line = line.expect("Error reading the line");

        let split = line.split(" ");
        let text_vec: Vec<&str> = split.collect();
        let instruction = text_vec[0];
        let ammount: i64 = text_vec[1].parse().expect("Error parsing input number");
        match instruction {
            FORWARD_TXT => submarine.forward(ammount),
            DOWN_TXT => submarine.down(ammount),
            UP_TXT => submarine.up(ammount),
            _ => println!("Instruction not recognised : {}", instruction),
        }
    }

    println!("Mult part two : {}", submarine.get_mult());
}
