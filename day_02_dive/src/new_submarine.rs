#[derive(Debug)]
pub struct NewSubmarine {
    depth: i64,
    h_pos: i64,
    aim: i64,
}

impl NewSubmarine {
    pub fn new() -> NewSubmarine {
        NewSubmarine {
            depth: 0,
            h_pos: 0,
            aim: 0,
        }
    }

    pub fn up(&mut self, delta: i64) {
        self.aim -= delta;
    }

    pub fn down(&mut self, delta: i64) {
        self.aim += delta;
    }
    pub fn forward(&mut self, delta: i64) {
        self.h_pos += delta;
        self.depth += self.aim * delta;
    }

    pub fn get_mult(&self) -> i64 {
        self.depth * self.h_pos
    }
}
