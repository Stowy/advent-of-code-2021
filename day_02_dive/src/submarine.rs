#[derive(Debug)]
pub struct Submarine {
    depth: i32,
    h_pos: i32,
}

impl Submarine {
    pub fn new() -> Submarine {
        Submarine { depth: 0, h_pos: 0 }
    }

    pub fn up(&mut self, delta: i32) {
        self.depth -= delta;
    }

    pub fn down(&mut self, delta: i32) {
        self.depth += delta;
    }

    pub fn forward(&mut self, delta: i32) {
        self.h_pos += delta;
    }

    pub fn get_mult(&self) -> i32 {
        self.depth * self.h_pos
    }
}
