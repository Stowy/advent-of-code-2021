use std::collections::HashMap;
use std::fs;

const FILENAME: &str = "smol_input.txt";

fn main() {
    // Read the file
    let content = fs::read_to_string(FILENAME).expect("Error opening the input file.");

    // Split the file by lines
    let lines: Vec<&str> = content.lines().collect();
    // let lines2 = lines.clone();
    let start_time = std::time::Instant::now();
    solve_part_one(lines);
    let time1ns = start_time.elapsed().as_nanos();
    let time1ms = start_time.elapsed().as_millis();
    println!("Part 1 = {} ns, {} ms\n", time1ns, time1ms);

    // let start_time = std::time::Instant::now();
    // solve_part_two(lines2);
    // let time2ns = start_time.elapsed().as_nanos();
    // let time2ms = start_time.elapsed().as_millis();
    // println!("Part 2 = {} ns, {} ms", time2ns, time2ms);
}

fn solve_part_one(input: Vec<&str>) {
    let mut map: HashMap<&str, Vec<&str>> = HashMap::new();
    for line in input {
        // Parse line
        let mut splited = line.split('-');
        let left = splited.next().unwrap();
        let right = splited.next().unwrap();

        // Add data to hashmap
        if map.contains_key(left) {
            let map_vec = map.get_mut(left).unwrap();
            map_vec.push(right);
        } else {
            map.insert(left, vec![right]);
        }
    }

    for path in map.get("start").unwrap() {}

    dbg!(map);
}

// fn solve_part_two(input: Vec<&str>) {}
