use std::fs;

const FILENAME: &str = "input.txt";

fn main() {
    // Read the file
    let content = fs::read_to_string(FILENAME).expect("Error opening the input file.");

    // Split the file by lines
    let lines: Vec<&str> = content.lines().collect();
    let lines2 = lines.clone();
    let start_time = std::time::Instant::now();
    solve_part_one(lines);
    let time1ns = start_time.elapsed().as_nanos();
    let time1ms = start_time.elapsed().as_millis();
    println!("Part 1 = {} ns, {} ms\n", time1ns, time1ms);

    let start_time = std::time::Instant::now();
    solve_part_two(lines2);
    let time2ns = start_time.elapsed().as_nanos();
    let time2ms = start_time.elapsed().as_millis();
    println!("Part 2 = {} ns, {} ms", time2ns, time2ms);
}

fn solve_part_one(input: Vec<&str>) {
    let mut num_counts: [i32; 10] = [0; 10];
    for line in input {
        let splitted = line.split(" | ");
        let output_values_str = splitted.last().unwrap();
        let output_values = output_values_str.split(' ');
        for value in output_values {
            if value.len() == 2 {
                num_counts[1] += 1;
            } else if value.len() == 3 {
                num_counts[7] += 1;
            } else if value.len() == 4 {
                num_counts[4] += 1;
            } else if value.len() == 7 {
                num_counts[8] += 1;
            }
        }
    }

    println!(
        "1 : {}, 4 : {}, 7 : {}, 8 : {}",
        num_counts[1], num_counts[4], num_counts[7], num_counts[8]
    );
    println!(
        "{}",
        num_counts[1] + num_counts[4] + num_counts[7] + num_counts[8]
    );
}

fn solve_part_two(input: Vec<&str>) {
    let mut global_sum = 0;
    for line in input {
        let mut input_nums: [&str; 10] = [""; 10];
        // Split intput and output values
        let mut splitted = line.split(" | ");
        // Parse input values
        let input_values_str = splitted.next().unwrap();
        let input_values: Vec<&str> = input_values_str.split(' ').collect();

        // Parse output values
        let output_values_str = splitted.next().unwrap();
        let output_values = output_values_str.split(' ');

        // Find coutable nums
        input_nums[1] = input_values.iter().find(|value| value.len() == 2).unwrap();
        input_nums[4] = input_values.iter().find(|value| value.len() == 4).unwrap();
        input_nums[7] = input_values.iter().find(|value| value.len() == 3).unwrap();
        input_nums[8] = input_values.iter().find(|value| value.len() == 7).unwrap();

        // Find 6
        for value in &input_values {
            if value.len() == 6 {
                let mut char_count = 0;
                for one_char in input_nums[1].chars() {
                    for other_char in value.chars() {
                        if one_char == other_char {
                            char_count += 1;
                        }
                    }
                }

                if char_count == 1 {
                    input_nums[6] = value;
                }
            }
        }

        // Find 9
        for value in &input_values {
            if value.len() == 6 {
                let mut char_count = 0;
                for one_char in input_nums[4].chars() {
                    for other_char in value.chars() {
                        if one_char == other_char {
                            char_count += 1;
                        }
                    }
                }

                if char_count == 4 {
                    input_nums[9] = value;
                }
            }
        }

        // Find 0
        for value in &input_values {
            if value.len() == 6 {
                if *value != input_nums[6] && *value != input_nums[9] {
                    input_nums[0] = value;
                }
            }
        }

        // Find 2
        for value in &input_values {
            if value.len() == 5 {
                let mut char_count = 0;
                for one_char in input_nums[4].chars() {
                    for other_char in value.chars() {
                        if one_char == other_char {
                            char_count += 1;
                        }
                    }
                }

                if char_count == 2 {
                    input_nums[2] = value;
                }
            }
        }

        // Find 3
        for value in &input_values {
            if value.len() == 5 {
                let mut char_count = 0;
                for one_char in input_nums[2].chars() {
                    for other_char in value.chars() {
                        if one_char == other_char {
                            char_count += 1;
                        }
                    }
                }

                if char_count == 4 {
                    input_nums[3] = value;
                }
            }
        }

        // Find 5
        for value in &input_values {
            if value.len() == 5 {
                if *value != input_nums[3] && *value != input_nums[2] {
                    input_nums[5] = value;
                }
            }
        }

        let mut num: String = String::new();

        for output in output_values {
            for i in 0..input_nums.len() {
                // Check if same letter
                let mut char_count = 0;
                output.chars().for_each(|c| {
                    if input_nums[i].contains(c) {
                        char_count += 1
                    }
                });
                if output.len() == input_nums[i].len() && output.len() == char_count {
                    num.push_str(&i.to_string());
                    break;
                }
            }
        }
        global_sum += num.parse::<i32>().unwrap();
    }

    println!("Part 2 sum : {}", global_sum);
}
